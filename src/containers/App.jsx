import React, { useState,useEffect }from 'react';
import Header from '../components/Header';
import Search from '../components/Search';
import Categories from '../components/Categories';
import Carousel from '../components/Carousel';
import CarouselItem from '../components/CarouselItem';
import useInitialState from '../hooks/useInitialState';
import '../assets/styles/App.scss';

const App = () => {
const initialState =   useInitialState('http://localhost:3000/initalState');  
    return initialState.length === 0 ? <h1>...Loading</h1>:(
        <div className="App">
            <Header/>
            <Search/>
            {/* Esto es como un if */}
            {initialState.mylist.length > 0 && 
                <Categories title="Mi lista">
                    <Carousel>
                        <CarouselItem/>
                        <CarouselItem/>
                        <CarouselItem/>
                    </Carousel>
                </Categories>
            }

            <Categories title="Tendencias">
                    <Carousel>
                    {/* Esto es un for */}
                    {initialState.trends.map(item => 
                        <CarouselItem key={item.id} { ...item} />
                    )}
                    </Carousel>
            </Categories>

            <Categories title="Originales">
                    <Carousel>
                    {/* Esto es un for */}
                    {initialState.originals.map(item => 
                        <CarouselItem key={item.id} { ...item} />
                    )}
                    </Carousel>
            </Categories>
        </div>

    )
};
export default App;