import { useState,useEffect }from 'react';
/**
 * 
 * @param API Ruta de la Api  
 */
function useInitialState(API){
    const [ videos,setVideos ] = useState([]);
    useEffect(()=>{
        fetch(API)
        .then(response => response.json())
        .then(data => setVideos(data));
    },[]);
    //El arreglo vacio en el 2do parametro de useEffect hace que no se genere un bucle infinito (sin ese parametro no sale de la funcion)
    return videos;
}

export default useInitialState;