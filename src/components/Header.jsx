import React from 'react';
import logo from '../assets/Statics/logo-platzi-video-BW2.png';
import icono from '../assets/Statics/user-icon.png';
import '../assets/styles/Header.scss'
const Header = ()=>(
    <header className="header">
        <img className="header__img" src={logo} alt="Platzi Video"/>
        <div className="header__menu">
            <div className="header__menu--profile">
                <img src={icono} alt="Icono"/>
                <p>Perfil</p>
            </div>
            <ul>
                <li><a href="/">Cuenta</a></li>
                <li><a href="/">Cerrar Sesión</a></li>
            </ul>
        </div>
    </header>         
);

export default Header;