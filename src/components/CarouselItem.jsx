import React from 'react';
import propTypes from 'prop-types';
import '../assets/styles/CarouselItem.scss';
import playIcon from '../assets/Statics/play-icon.png';
import plusIcon from '../assets/Statics/plus-icon.png';

const CarouselItem = ({ cover,title,year,contentRating,duration })=> (
    <div className="carousel-item">
        <img className="carousel-item__img" src="https://images.pexels.com/photos/789822/pexels-photo-789822.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260" alt=""  />
        <div className="carousel-item__details">
            <div>             
            <img className="carousel-item__details--img" src={playIcon} alt="Play Icon"/>
            <img className="carousel-item__details--img" src={plusIcon} alt="Plus Icon"/>
            </div>
            <p className="carousel-item__details--title">{title}</p>
            <p className="carousel-item__details--subtitle">{year} {contentRating} {duration} Minutos</p>
        </div>
    </div>
);

CarouselItem.propTypes = {
    cover           :propTypes.string,
    title           :propTypes.string,
    year            :propTypes.number,
    contentRating   :propTypes.string,
    duration        :propTypes.number
}
//Esta es la forma en la que react tiene para decirle que un componente espera un tipo de dato especifico en sus parametros
//se Necesita instalar el paquete npm prop-types
export default CarouselItem;